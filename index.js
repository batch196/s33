//express package imported
const express = require("express");

const app = express();


//express.json is a method from express that allows us to handle the stream of data from our client and recieve the data and automatically parse the incomming json from the request.
//app.use is a method to from express js to run another function or method.
app.use(express.json())

const port = 4000;

//mock collection for courses
let courses = [
	
	{
		name: "python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn React",
		price: 35000
	},
	{
		name: "ExpressJS",
		description: "Learn ExpressJS",
		price: 28000
	},

];

let users= [
	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimTufo",
		password: "duboTufo"
	}

]


//creating a root in express
app.get('/',(req,res)=>{

	res.send("Hello from our very first ExpressJS route")
})

app.post('/',(req,res)=>{
	res.send("Hello from our first ExpressJS POST Method");
})
app.put('/',(req,res)=>{
	res.send("Hello from a put method route");
})

app.delete("/",(req,res)=>{
	res.send("Hello from a delete Method route")
})

app.get('/courses',(req,res)=>{
	res.send(courses);
})

app.post('/courses',(req,res)=>{
	console.log(req.body);
	courses.push(req.body);
	res.send(courses);
})

app.get('/users',(req,res)=>{

	res.send(users);
})
app.post('/users',(req,res)=>{
	console.log(req.body);
	users.push(req.body);
	res.send(users);

})


app.listen(port,() => console.log(`Express API running at port 4000`))


